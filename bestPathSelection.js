
router.post('/router', (req, res) => {

    rout.find({}, (err, routers) => {
        const dest = req.body.destination;

        const src = '192.168.0.5';
        const msg = req.body.message;
        console.log(`-------Sending message ${msg} from ${src} to ${dest} --------`);
        console.log('  ');

        let r1 = {};
        let r2 = {};
        let r3 = {};
        for (let i = 0; i < routers.length; i++) {
            r1 = routers[i];
            i = i + 1;
            r2 = routers[i];
            i = i + 1;
            r3 = routers[i];
        }

        let r1interfaces = [];

        let r2interfaces = [];
        let r3interfaces = [];
        let j = 0;
        for (j = 0; j < routers.length; j++) {
            r1interfaces = routers[j].interfaces;
            j = j + 1;
            r2interfaces = routers[j].interfaces;
            j = j + 1;
            r3interfaces = routers[j].interfaces;
        }

        const inter1 = [];

        for (let x = 0; x < r1interfaces.length; x++) {
            inter1.push(r1interfaces[x].int1);
            inter1.push(r1interfaces[x].int2);
            inter1.push(r1interfaces[x].int3);
        }

        let cost = 0;
        let cost1 = 0;
        let cost32 = 0;
        let via = '';
        let res = false;

        for (let y = 0; y < inter1.length; y++) {
            if (dest.split('.')[0] == inter1[y].split('.')[0]) {
                cost = 0;
                via = inter1[y];
                res = true;
                console.log(`cost = ${cost} via network ${via}`);
                break;
            } else {
                cost1 = cost1 + 1;
                cost32 = cost32 + 1;
                break;
            }
        }

        const inter2 = [];
        for (let y = 0; y < r2interfaces.length; y++) {
            inter2.push(r2interfaces[y].int1);
            inter2.push(r2interfaces[y].int2);
            inter2.push(r2interfaces[y].int3);
        }


        const array = [];
        for (let m = 0; m < inter2.length; m++) {
            array.push(inter2[m].split('.')[0]);
        }

        let via12 = '';
        for (let k = 0; k < inter2.length; k++) {
            if (array.includes(dest.split('.')[0])) {
                for (let z = 0; z < inter1.length; z++) {
                    if (inter2[k].split('.')[0] == inter1[z].split('.')[0]) {
                        cost1 = cost1 + 1;
                        via12 = inter1[z];
                        console.log(`pinging ${dest} via ${via12} with cost ${cost1}`);
                    }
                }
            }
        }

        const inter3 = [];
        for (let z = 0; z < r1interfaces.length; z++) {
            inter3.push(r3interfaces[z].int1);
            inter3.push(r3interfaces[z].int2);
            inter3.push(r3interfaces[z].int3);
        }

        const array3 = [];
        for (let m = 0; m < inter3.length; m++) {
            array3.push(inter3[m].split('.')[0]);
        }
        // console.log(array3);

        let viam = '';
        for (let t = 0; t < inter3.length; t++) {
            if (array3.includes(dest.split('.')[0])) {
                for (let z = 0; z < inter1.length; z++) {
                    if (inter3[t].split('.')[0] == inter1[z].split('.')[0]) {
                        cost32 = cost32 + 1;
                        viam = inter1[z];
                        console.log(`pinging ${dest} via ${viam} with cost ${cost32}`);
                    }
                }
            } else if (!array3.includes(dest.split('.')[0])) {
                for (let b = 0; b < inter1.length; b++) {
                    if (inter3[t].split('.')[0] == inter1[b].split('.')[0]) {
                        cost32 = cost32 + 1;
                    }
                }
                if (array.includes(dest.split('.')[0])) {
                    for (let c = 0; c < inter2.length; c++) {
                        if (inter2[c].split('.')[0] == inter3[t].split('.')[0]) {
                            cost32 = cost32 + 1;
                            viam = inter3[t];
                            // console.log(
                            //     `can be connected via ${viam} with cost ${cost32} but couldn't be`
                            // );
                        }
                    }
                }
            }
        }

        // destination validate
        var status = 0;
        var first = dest.split('.')[0];
        for (let i = 0; i < inter1.length; i++) {
            if (inter1[i].split('.')[0] == first) {
                status = 1;
                break;
            }
        }
        for (let i = 0; i < inter2.length; i++) {
            if (inter2[i].split('.')[0] == first) {
                status = 1;
                break;
            }
        }
        for (let i = 0; i < inter3.length; i++) {
            if (inter3[i].split('.')[0] == first) {
                status = 1;
                break;
            }
        }
        if (status) {
            // console.log(cost32);
            const x = cost1 < cost32 ? cost1 : cost32;
            let through = '';
            if (res) {
                through = via;
            } else {
                through = x === 2 ? via12 : viam;
            }
            console.log(`Message ${msg} has successfuly sent to ${dest} through ${through} with cost ${x}`)
        } else {

            console.log('destination unreachable');
        }
    });

});