require('./models/db');
var nexmo = require('nexmo');
const express = require('express');
var User = require('./models/userAccount.model');
var img = require('./models/image.model');
var app = express();
const path = require('path');
var fs = require("fs");
var multer = require("multer");
//var upload = multer();
const exphbs = require("express-handlebars");
const bodyparser = require("body-parser");
var cookieParser = require("cookie-parser");
var flash = require("connect-flash");
var passport = require("passport");
var nodemailer = require('nodemailer');
var async = require("async");
var crypto = require("crypto");
var session = require('express-session');
var validator = require('express-validator');
var bcrypt = require('bcrypt-nodejs');
var io = require('socket.io');


const userController = require('./controllers/userController');
const homeController = require('./controllers/homeController');
const routes = require('./routes/pages');
//var router = express.Router();
//router.use("/hello", routes);
app.use(bodyparser.urlencoded({
    extended: true
}));
//app.use(upload.array());
app.use(bodyparser.json());
// var csrf = require("csurf");
// var csrfProtection = csrf({ cookie: true });
require('./config/passport');
app.use(cookieParser());
app.use(session({ secret: 'mysupersecrert', resave: false, saveUninitialized: false }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, '/public')));
app.set('views', path.join(__dirname, '/views'));
var user = require('./models/userAccount.model');
var router = require('./models/routerModel.js');
app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'mainlayout', layoutsDir: __dirname + '/views/layouts/' }));

// app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'dashboared', layoutsDir: __dirname + '/views/layouts/' }));
app.set("view engine", 'hbs');
app.listen(8000, () => {
    console.log("express server started");
});

app.all("*", function(req, res) {
    //return res.json({ status: 205, message: 'no route found' });
    res.render('user/error');
});